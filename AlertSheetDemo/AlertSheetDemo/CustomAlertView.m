//
//  CustomAlertView.m
//  Project
//
//  Created by apple on 2018/4/3.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "CustomAlertView.h"

#define SCREEN_HEIGHT   ([UIScreen mainScreen].bounds.size.height)
#define SCREEN_WIDTH    ([UIScreen mainScreen].bounds.size.width)
#define Alert_Item_Height  50
#define Alert_Background_Tag 11001
#define Alert_Button_Tag 11010
#define Alert_bgView_Tag 11011



static CustomAlertView *customAlertView = nil;

@interface CustomAlertView()
{
    NSArray *dataArray;
}

@property (nonatomic,copy) void(^clickBlock)(NSInteger index);

@end

@implementation CustomAlertView

+ (instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        customAlertView = [[self alloc] init];
    });
    return customAlertView;
}

- (void)showAlertViewBy:(NSArray*)items view:(UIView *)view
{

    self.view_bg = view;

    CGFloat bgViewHeight;
    bgViewHeight = (items.count+1)*Alert_Item_Height+10+items.count;
    CGRect orginRect = CGRectMake(0, view.frame.size.height, SCREEN_WIDTH, bgViewHeight);
    CGRect finalRect = orginRect;
    finalRect.origin.y = view.frame.size.height-bgViewHeight;


//    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    //背景view
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, view.frame.size.height)];
    bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    bgView.tag = Alert_Background_Tag;
    bgView.userInteractionEnabled= YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:customAlertView action:@selector(dismissView)];
    [bgView addGestureRecognizer:tap];
    [self.view_bg addSubview:bgView];
    //模糊效果
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    effectView.frame = orginRect;
    effectView.userInteractionEnabled = YES;
    effectView.backgroundColor = [UIColor whiteColor];
    [bgView addSubview:effectView];

    //弹框的背景view
    UIView *alertBgView = [[UIView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, bgViewHeight)];
    alertBgView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.9];
    alertBgView.tag = Alert_bgView_Tag;
    [effectView.contentView addSubview:alertBgView];

    // 取消按钮
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(0, items.count*Alert_Item_Height+10+items.count, SCREEN_WIDTH, Alert_Item_Height)];
    [cancelButton setBackgroundColor:[UIColor whiteColor]];
    [cancelButton.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    [cancelButton setTitle:@"取消" forState:(UIControlStateNormal)];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [cancelButton addTarget:customAlertView action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    [alertBgView addSubview:cancelButton];

    //根据传入的数组生成对应的button
    for(int i = 0;i<items.count;i++){
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = Alert_Button_Tag + i;
        btn.backgroundColor = [UIColor whiteColor];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
        [btn setFrame:CGRectMake(0, i*(Alert_Item_Height+1), SCREEN_WIDTH, Alert_Item_Height)];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitle:[[NSString alloc] initWithFormat:@"%@",items[i]] forState:UIControlStateNormal];
        [btn addTarget:customAlertView action:@selector(alertButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [alertBgView addSubview:btn];
    }
    [effectView.contentView addSubview:alertBgView];

    //动画弹出选项栏
    effectView.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        effectView.frame = finalRect;
        effectView.alpha = 1;
    }];
}


- (void)alertButtonClick:(UIButton*)sender
{
    NSInteger tag = sender.tag-Alert_Button_Tag;
    if(self.delegate){
        [self.delegate actionAlert:self clickedButtonAtIndex:tag];
    }
    [self dismissView];
}

- (void)dismissView
{
    UIView *alertBgView = [self.view_bg viewWithTag:Alert_bgView_Tag];
    UIView *blackView = [self.view_bg viewWithTag:Alert_Background_Tag];
    [UIView animateWithDuration:0.3
                     animations:^{
                         blackView.alpha = 0;
                         CGRect alertFrame = [alertBgView frame];
                         alertFrame.origin.y = SCREEN_HEIGHT;
                         alertBgView.frame = alertFrame;
                     } completion:^(BOOL finished) {
                         [blackView removeFromSuperview];
                     }];
}

@end
