//
//  ViewController.m
//  AlertSheetDemo
//
//  Created by apple on 2019/2/28.
//  Copyright © 2019 jacky. All rights reserved.
//

#import "ViewController.h"
#import "CustomAlertView.h"

@interface ViewController ()<CustomAlertViewDelegate>

@property (nonatomic,strong) CustomAlertView *alertV;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    _alertV = [CustomAlertView shareInstance];
    _alertV.delegate = self;
}

- (IBAction)clickAction:(id)sender {
    [[CustomAlertView shareInstance] showAlertViewBy:@[@"1",@"2",@"3",@"4"] view:self.view];
}

- (void)actionAlert:(CustomAlertView *)actionAlert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"点击了 %ld",buttonIndex);
}

@end
