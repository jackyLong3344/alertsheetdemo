//
//  CustomAlertView.h
//  Project
//
//  Created by apple on 2018/4/3.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CustomAlertView;
@protocol CustomAlertViewDelegate <NSObject>

- (void)actionAlert:(CustomAlertView *)actionAlert clickedButtonAtIndex:(NSInteger)buttonIndex;

@end


@interface CustomAlertView : UIView

+ (instancetype)shareInstance;

/**
*param items 传入标题数组
*param block 点击对应的标题的回调
**/
- (void)showAlertViewBy:(NSArray*)items view:(UIView *)view;

/**
 弹窗隐藏
 */
- (void)dismissView;

// 在这里定义一个属性，注意这里的修饰词要用weak
@property(nonatomic,weak)id<CustomAlertViewDelegate>  delegate;

@property (nonatomic, strong) UIView *view_bg;

@end
